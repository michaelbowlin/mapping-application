Angular Base
=====================================

The Angular Base platform tool front end using AngularJS, SASS, Gulp, and Browserify 
that also utilizes [these best AngularJS practices](https://github.com/toddmotto/angularjs-styleguide) and Gulp best practices 
from [this resource](https://github.com/greypants/gulp-starter).

---

### Getting up and running

#### Dependencies

Before building the app, ensure the following dependencies are installed on 
the build machine:

* [NodeJS](http://nodejs.org/) - Used by the build system and local web server
  * Visit NodeJS's website for install instructions
* [Gulp](http://gulpjs.com/) - The build system task runner
  * With node installed, run `npm install gulp -g` to install Gulp globally

#### Checkout, Build, and Develop

1. Clone this repo
2. Run `npm install` from the root directory
3. Run `gulp local` (needs `Gulp` installed globally - see above)
  * Your browser will automatically be opened and directed to the browser-sync proxy address
4. To prepare assets for production, run the `gulp prod` task (Note: the production task does not fire up the express server, and won't provide you with browser-sync's live reloading. Use `gulp local` during development, and `gulp prod` for bundling for production. See below for more information.)

Now that `gulp local` is running, the server is up as well and serving files from 
the `/build` directory. Any changes in the `/app` directory will be 
automatically processed by Gulp and the changes will be injected to any open 
browsers pointed at the proxy address.

---

The framework uses the latest versions of the following libraries:

- [AngularJS](http://angularjs.org/)
- [SASS](http://sass-lang.com/)
- [Gulp](http://gulpjs.com/)
- [Browserify](http://browserify.org/)

Along with many Gulp libraries (these can be seen in either `package.json`, or 
at the top of each task in `/gulp/tasks/`).

---

### AngularJS

AngularJS is a MVW (Model-View-Whatever) Javascript Framework for creating single-page web applications. In this framework, it is used for all of the application routing as well as all of the frontend views and logic.

The AngularJS files are all located within `/app`, and are structured as follows, based on [Google's best practice guidelines for Angular app structure](https://docs.google.com/document/d/1XXMvReO8-Awi1EZXAXS4PzDzdNvV6pGcuaF4Q9821Es/pub):

```
/components
  _index.js               (the main module on which to mount all component modules, loaded 
                          in app.js)
  /example
    _index.js             (each module uses _index.js to mount its component pieces)
    example.directive.js  (a simple example directive for the example module)
    example.service.js    (a simple example service for the example module)
  /footer
    _index.js             (the mount file for the footer module)
    footer.controller.js  (the controller for the footer module)
  /navbar
    _index.js             (the mount file for the navbar module)
    navbar.controller.js  (the controller for the navbar module)
/config
  constants.js            (application settings used globally, e.g. appTitle, etc.)
  envConstants.js         (environment-specific - local, qa, prod, etc. - configuration, e.g. 
                          Google Analytics tracking, API host, etc. added to the build by 
                          the respective task and should not be modified as it will 
                          be overwritten)
  /envs
    envConstants.dev.js   (environment-spcific configuration for the AWS development server, e.g. 
                          Google Analytics tracking, etc.)
    envConstants.qa.js    (environment-spcific configuration for the AWS qa server, e.g. 
                          Google Analytics tracking, etc.)
    envConstants.stage.js (environment-spcific configuration for the AWS staging server, 
                          e.g. Google Analytics tracking, etc.)
    envConstants.prod.js  (environment-spcific configuration for the AWS production server, 
                          e.g. Google Analytics tracking, etc.)
  on_run.js               (application runtime settings, e.g. pageTitle, etc.)
  routes.js               (default Angular route, HTML5 mode, etc. not covered by ui-router)
/home
  _index.js               (the mount file for the home module)
  home.config.js          (the config block for the home module, including ui-router state config)
  home.controller.js      (the controller for the home module)
app.js                    (the main file read by Browserify; also where the application 
                          is defined and bootstrapped)
templates.js              (this is created via Gulp by compiling your views, and will not 
                          be present beforehand, nor should it be modified as it will 
                          be overwritten on every re-bundle)
```

All components that are not a specific application state (e.g. `home`, `demos`, `intro`, etc.) should be self-contained and added to `/components`. Controllers, services, directives, etc. should all be placed within their respective module folders and mounted on their respective `_index.js` module. Most other logic can be placed in an existing file, or added in new files as long as it is required inside `app.js`.

#### Dependency injection

Dependency injection is carried out with the `ng-annotate` library. In order to take advantage of this, a simple comment of the format:

```
/**
 * @ngInject
 */
```

needs to be added directly before any Angular functions/modules. The Gulp tasks will then take care of adding any dependency injection, requiring you only to specify the dependencies within the function call and nothing more.

---

### SASS

SASS, standing for 'Syntactically Awesome Style Sheets', is a CSS extension language adding things like extending, variables, and mixins to the language. Supervillain provides a barebones file structure for your styles, with explicit imports into `app/app.scss`. A Gulp task is provided for compilation and minification of the stylesheets based on this file.

SCSS files for vendor libraries are included in `app/vendor.scss`.

### Bootstrap and UI Bootstrap

The view structure is built using the [Bootstrap](http://getbootstrap.com/) framework. Bootstrap's javascript is provided for native AngularJS support by [UI Bootstrap](http://angular-ui.github.io/bootstrap/), and its directives should be used when leveraging Bootstrap's javascript components.

---

### Browserify

Browserify is a Javascript file and module loader, allowing you to leverage CommonJS modules. `require('modules')` in all of your files in the same manner as you would on the backend in a node.js environment. The bundling and compilation is then taken care of by Gulp, discussed below.

---

### Gulp

[Gulp](http://gulpjs.com/) is a "streaming build system", providing a very fast and efficient method for running your build tasks.

##### Web Server

Gulp is used here to provide a very basic node/Express web server for viewing and testing your application as you build. It serves static files from the `/build` directory, leaving routing up to AngularJS. All Gulp tasks are configured to automatically reload the server upon file changes. The application is served to `localhost:3000` once you run the `gulp local` task. To take advantage of the fast live reload injection provided by browser-sync, you must load the site at the proxy address (which usually defaults to `server port + 1`, and within this platform will by default be `localhost:3001`.)

##### Scripts

A number of build processes are automatically run on all of of our Javascript files in the following order:

- **NGDocs:** API documentation is automatically generated using the [gulp-ngdocs](https://github.com/nikhilmodak/gulp-ngdocs) plugin. API documentation resides in the `/docs` directory.
- **JSHint:** Gulp is currently configured to run a JSHint task before processing any Javascript files. This will show any errors in your code in the console, but will not prevent compilation or minification from occurring.
- **Browserify:** The main build process run on any Javascript files. This processes any of the `require('module')` statements, compiling the files as necessary.
- **Babelify:** This uses [BabelJS](https://babeljs.io) to privide support for ES6+ development.
- **Debowerify:** Parses `require()` statements in your code, mapping them to `bower_components` when necessary. This allows you to use and include bower components just as you would npm modules.
- **ngAnnotate:** This will automatically add the correct dependency injection to any AngularJS files, as mentioned above.
- **Uglify:** This will minify the file created by Browserify and ngAnnotate.

The resulting file (`app.js`) is placed inside the directory `/build`.

##### Styles

One task, `gulp-sass`, is necessary for processing our SASS files. This will read the `app.scss` file, processing and importing any dependencies and then minifying the result. This file (`app.css`) is placed inside the directory `/build`.

- **gulp-autoprefixer:** Gulp is currently configured to run autoprefixer after compiling the scss. Autoprefixer will use the data based on current browser popularity and property support to apply prefixes for you. Autoprefixer is recommended by Google and used in Twitter, WordPress, Bootstrap and CodePen.

##### Images

Any images placed within `/app/images` will be automatically copied to the `build/images` directory. If running `gulp prod`, they will also be compressed via imagemin.

##### Views

When any changes are made to the `index.html` file, the new file is copied to the `/build` directory without any changes occurring.

All other HTML files within `/app`, on the other hand, go through a slightly more complex process. The `gulp-angular-templatecache` module is used in order to process all views/partials, creating the `template.js` file mentioned above. This file will contain all the views, now in Javascript format, inside Angular's `$templateCache` service. This will allow us to include them in our Javascript minification process, as well as avoid extra HTTP requests for our views.

##### Watching files

All of the Gulp processes mentioned above are run automatically when any of the corresponding files in the `/app` directory are changed using Gulp watch tasks. Running `gulp local` will begin watching all of these files, serving to `localhost:3000`, with browser-sync proxy running at `localhost:3001` (by default).

##### Dev, QA, Stage, and Production Tasks

There are 4 compiled builds available, 1 for each AWS-served environment: `gulp dev`, `gulp qa`, `gulp stage`, and `gulp prod`. Each of these tasks prepares the app for a production-ready compiled state. This will run each of the tasks, while also adding the image minification task mentioned above and pulling-in environment-specific configuration (e.g. Google Analytics tracking, API host, etc.). There is also an empty `gulp deploy` task that can be wired to these tasks and can be fleshed out to automatically push the compiled site to the AWS environment.

When running one of the compiled tasks, pre-compressed and uncompressed files are generated (`html.gz`, `js.gz`, and `css.gz`) using the `gzip` task. This enables web servers to serve compressed content without having to compress it on the fly.

**Reminder:** When running one of these tasks, gulp will not fire up the express server and serve the site. These are designed to be run before the `deploy` step that may copy the files from `/build` to a production web server, as an example implementation of an automated deployment process.

##### Testing

Gulp tasks also exist for running the test framework (see below). Running `gulp test` will run any and all tests inside the `/test` directory and show the results (and any errors) in the terminal.

---

### Testing

The platform also includes a simple framework for unit and end-to-end (e2e) testing via [Karma](http://karma-runner.github.io/) and [Jasmine](http://jasmine.github.io/). In order to test AngularJS modules, the [angular.mocks](https://docs.angularjs.org/api/ngMock/object/angular.mock) module is used.

All of the tests can be run at once with the command `gulp test`. However, the tests are broken up into two main categories:

##### End-to-End (e2e) Tests

e2e tests, as hinted at by the name, consist of tests that involve multiple modules or require interaction between modules, similar to integration tests. These tests are carried out using the Angular library [Protractor](https://github.com/angular/protractor), which also utilizes Jasmine. The goal is to ensure that the flow of the application is performing as designed from start to finish.

Examples can be seen at the above link for Protractor.

All e2e tests are run with `gulp test:protractor`. The command `npm run-script preprotractor` should be run once before running any Protractor tests (in order to update the webdrivers used by Selenium).

**Notes:**

- the Protractor library used for the end-to-end tests may require installing the [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html) beforehand.

##### Unit Tests

Unit tests are used to test a single module (or "unit") at a time in order to ensure that each module performs as intended individually. In AngularJS this could be thought of as a single controller, directive, filter, service, etc. That is how the unit tests are organized in the platform.

Testing AngularJS directives becomes a bit more complex involving mock data and DOM traversal. Learn more [here](http://newtriks.com/2013/04/26/how-to-test-an-angularjs-directive/).

All unit tests are run with `gulp test:unit`. When run, unit test code coverage is generated using `Istanbul` and output to the `/coverage` directory.

---

### Help

A help task exists, `gulp help`, that will output a list of all available gulp tasks grouped by `Main Tasks` and `Sub Tasks`. Note that all tasks can be run independently, e.g. `gulp build:clean`, however the main tasks are built to accomplish a larger, overall goal leveraging the sub tasks.

'use strict';

var config = require('../config');
var gulp   = require('gulp');
var rename = require('gulp-rename');

gulp.task('build:envconf', function () {

  var env = global.env ? global.env : 'prod';

  return gulp.src(config.envconf.src + 'envConstants.' + env + '.js')
    .pipe(rename('envConstants.js'))
    .pipe(gulp.dest(config.envconf.dest));

});
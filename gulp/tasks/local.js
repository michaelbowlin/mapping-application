'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');

gulp.task('local', ['build:clean'], function (cb) {

  cb = cb || function () {};

  global.env    = 'local';
  global.isProd = false;

  runSequence(
    'build:envconf',
    [
      'build:document',
      'build:styles:app',
      'build:styles:vendor',
      'build:fonts',
      'build:images',
      'build:views'
    ],
    'build:browserify',
    'build:watch',
    cb
  );

});
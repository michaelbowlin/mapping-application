'use strict';

var config = require('../config');
var gulp   = require('gulp');
var jshint = require('gulp-jshint');

gulp.task('build:lint', function () {

  return gulp.src([
      config.scripts.src,
      '!app/templates.js',
      '!app/materialize/googleAnalytics/googleAnalytics.directive.js',
      '!app/materialize/**/test/*.js',
      '!app/config/test/*.js'
    ])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
    
});
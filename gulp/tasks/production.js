'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');

gulp.task('prod', ['build:clean', 'build:document'], function (cb) {

  cb = cb || function () {};

  global.isProd = true;

  runSequence(
    'build:envconf',
    [
      'build:fonts',
      'build:images',
      'build:views'
    ],
    'build:replaceVersions',
    'build:gzip',
    cb
  );

});
'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');

gulp.task('stage', function (cb) {

  cb = cb || function () {};

  global.env    = 'stage';

  runSequence(
    'prod',
    cb
  );

});
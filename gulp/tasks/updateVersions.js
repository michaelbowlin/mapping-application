'use strict';

var config     = require('../config');
var gulp       = require('gulp');
var rev        = require('gulp-rev');
var revReplace = require('gulp-rev-replace');

// Update hash on the compiled JS and CSS files to force reload and avoid
// browser caching issues
gulp.task('build:updateHash',
  [
    'build:styles:app',
    'build:styles:vendor',
    'build:browserify'
  ],
  function () {
    
    return gulp.src([
        config.styles.dest + '/*.css',
        config.scripts.dest + '/*.js'
      ])
      .pipe(rev())
      .pipe(gulp.dest(config.dist.root))
      .pipe(rev.manifest())
      .pipe(gulp.dest(config.dist.root));
  
  }
);

// Replace the reference to the versioned files in index.html
gulp.task('build:replaceVersions', ['build:updateHash'], function () {

  var manifest = gulp.src(config.dist.root + '/rev-manifest.json');
    
  return gulp.src(config.dist.root + '/index.html')
    .pipe(revReplace({manifest: manifest}))
    .pipe(gulp.dest(config.dist.root));
  
});
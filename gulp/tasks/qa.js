'use strict';

var gulp        = require('gulp');
var runSequence = require('run-sequence');

gulp.task('qa', function (cb) {

  cb = cb || function () {};

  global.env    = 'qa';

  runSequence(
    'prod',
    cb
  );

});
/* global browser, by */

'use strict';

describe('Intro Route', function () {

  beforeEach(function () {
    browser.get('/');
    browser.waitForAngular();
  });

  it('should route correctly', function () {
    expect(browser.getLocationAbsUrl()).toMatch('/');
  });

});
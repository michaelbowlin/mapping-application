'use strict';

const LOCALMESSAGES = new WeakMap();
const Q = new WeakMap();
const STATEPARAMS = new WeakMap();
const TECHCHECK = new WeakMap();

/**
 * @ngdoc controller
 * @name  app.materialize.message.controller:MessageController
 * @description Controller for the message directive. Sets-up everything
 * the view needs to render the message.
 * @requires $q
 * @requires $stateParams
 * @requires LocalMsgs
 * @requires app.materialize.techCheck.service:techCheckService
 * 
 */
class MessageController {
  /** @ngInject */
  constructor( $q, $stateParams, LocalMsgs, techCheckService) {
    // injectables binding
    Q.set(this, $q);
    LOCALMESSAGES.set(this, LocalMsgs);
    STATEPARAMS.set(this, $stateParams);
    TECHCHECK.set(this, techCheckService);
    
    let vm = this;
    
    vm.errorCode = STATEPARAMS.get(this).errorCode;
    
    this._getMessage().then( (response) => {
      let message = response;

      vm.messageTitle = message.title;
      vm.messageBody = message.body;
    });
  }

  /** 
   * @ngdoc method
   * @name MessageController#_buildMessage
   * @methodOf app.materialize.message.controller:MessageController
   * @description Produces the formatted message object for display on the 
   * message state UI
   * @param {Object} msgObj The message object to be converted to a UI-friendly
   * message object
   * @param {String} errorCode The error code to load from the message object
   */
  _buildMessage( msgObj, errorCode ) {

    let message = {
      title: msgObj[errorCode + '.title'],
      body: msgObj[errorCode + '.body']
    };

    if ( errorCode === 'BROWSER' ) {
      message = this._formatBrowserMessage( message );
    }
    
    return message;

  }
  
  /**
   * @ngdoc method
   * @name MessageController#_formatBrowserMessage
   * @methodOf app.materialize.message.controller:MessageController
   * @description formats the browser error messaging to change the tokens
   * to include browser name and minimum version needed
   * @param {Object} message The message object to be converted to a message
   * which contains the token replacements for browser name and minimum
   * required version
   */
  _formatBrowserMessage( message ) {
    let browserMatrix = TECHCHECK.get(this).getDesktopBrowserMatrix();
    let device = TECHCHECK.get(this).getDeviceSpecifications();
    
    message.body = message.body.replace(
      "{browser}",
      browserMatrix[ device.browserType ].label
    );
    message.body = message.body.replace(
      "{version}",
      browserMatrix[ device.browserType ].version
    );
    
    return message;
  }
  
  /**
   * @ngdoc method
   * @name MessageController#_getMessage
   * @methodOf app.materialize.message.controller:MessageController
   * @description Gets the message from either the i18n service or the local
   * collection of messages based on what was passed in with the state params.
   *
   * @returns {Object} message object with title, body, and redirect text
   */
  _getMessage() {

    let errorCode = STATEPARAMS.get(this).errorCode;
    let strings = {};
    let message = {};

    // Try to fetch the message strings from the API provided language pack;
    // otherwise, use the local messages
    return this._loadLocalization().then(
      (response) => {

        console.log(
          'The strings response & errorCode are: ',
          response,
          errorCode
        );

        strings = response;

        // Set the message display object
        message = this._buildMessage(strings, errorCode);

        return message;
      
      }, () => {

        // Grab the message strings from our contants
        strings = LOCALMESSAGES.get(this).message;

        // Set the message display object
        message = this._buildMessage(strings, errorCode);

        return message;
      }
    );

  }
  
  _loadLocalization() {

    // Grab the lang. pack loaded via the API
    // return LOCALIZATION.get(this).getPageStrings('message').then(
    //   (strings) => {
    //     FOOTERNAVSERVICE.get(this).updateLocalization('message');
    //
    //     return strings;
    //   }
    // );
    
    // TODO: update this when localization available
    // force loading of local strings for now
    var d = Q.get(this).defer();
    d.reject();
    return d.promise;
  }
}

module.exports = MessageController;

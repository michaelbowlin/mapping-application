/* global angular */

'use strict';

describe('Unit: Message Component', function () {

  var $compile;
  var ctrl;
  var element;
  var $httpBackend;
  var $rootScope;

  var mockLocalization = {
  };

  beforeEach(function () {
    // Instantiate the app module
    angular.mock.module('app');

    // Store references to $compile and $rootScope so they're available to all
    // tests in this describe block
    // Note that the injector unwraps the underscores from around parameter
    // names when matching
    angular.mock.inject(function (
      _$compile_,
      _$rootScope_,
      _$httpBackend_,
      _$q_
    ) {

      // Watch our mock requests
      $httpBackend = _$httpBackend_;

      // Compile a piece of HTML containing the directive
      var elm = angular.element('<gc-message></gc-message>');

      // Create and compile an element from our directive
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      element = $compile(elm)($rootScope);
      $rootScope.$apply();

      // Grab the controller (so we can test the controller, too)
      ctrl = element.controller('gcMessage');
    });
  });

  it('should have replaced the directive template with output', function() {
    expect( element.hasClass('gc-container') ).toBe( true );
  });

  it('controller should exist', function() {
    expect(ctrl).toBeDefined();
  });

  //TODO: add more tests - figure out how to mock state params
});
'use strict';

var MessageController = require('./message.controller');

/**
 * @ngdoc directive
 * @name app.materialize.message.directive:message
 * @restrict E
 * @description The directive that produces the error messaging output
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-message></data-gc-message>
 * </pre>
 * 
 * @ngInject
 */
function gcMessage() {

  return {
    bindToController: true,
    controller: MessageController,
    controllerAs: 'message',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/message/message.html'
  };
  
}

module.exports = gcMessage;
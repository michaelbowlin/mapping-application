'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.message
 * @description A simple module thats produces the error messaging output for the application
 */
module.exports = angular.module('app.components.message', [])
  .controller('MessageController',
    require('./message.controller'))
  .directive('gcMessage',
    require('./message.directive'));
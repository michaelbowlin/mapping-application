'use strict';

var HeaderController = require('./header.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.header.directive:header
 * @restrict E
 * @description The directive that handles the top header
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-header></data-gc-header>
 * </pre>
 *
 * @ngInject
 */
function gcHeader() {

  return {
    bindToController: true,
    controller: HeaderController,
    controllerAs: 'header',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/header/header.html'
  };

}

module.exports = gcHeader;


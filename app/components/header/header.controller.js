'use strict';

const AUTHSERVICE = new WeakMap();
const ROOTSCOPE = new WeakMap();
const STATE = new WeakMap();

/**
 * @ngdoc controller
 * @name  app.materialize.header.controller:HeaderController
 * @requires $rootScope
 * @requires $state
 * @requires app.materialize.authentication.service:authenticationService
 * @description Controller for the header module.
 *
 */
class HeaderController {
  /** @ngInject */
  constructor($rootScope, $state, authenticationService) {

    // setup injectibles
    ROOTSCOPE.set(this, $rootScope);
    STATE.set(this, $state);
    AUTHSERVICE.set(this, authenticationService);

    /** Set-up the view model */
    let vm = this;
    vm.handleLogout = this._handleLogout;
    
    // properties
    vm.isAuthed = false; // default to hidden

    this._setupLocalization();
    this._setupWatchers();
  }
  
  /**
   * @ngdoc method
   * @name HeaderController#_handleLogout
   * @methodOf app.materialize.header.controller:HeaderController
   * @description logout link click handler
   * @param {Object} evt the click event
   */
  _handleLogout( evt ) {
    evt.preventDefault();
    
    AUTHSERVICE.get(this).logout().then(
      () => {
        // TODO: handle any other logout stuff
      
        STATE.get(this).go('LOGIN');
      }
    );
  }

  /**
   * @ngdoc method
   * @name HeaderController#_setupLocalization
   * @methodOf app.materialize.header.controller:HeaderController
   * @description sets up the localization strings for the header
   */
  _setupLocalization() {

    // TODO: add localization service call when it's ready
    this.hello = "Hello";
    this.logout = "Log Out";
    this.myProfile = "My Profile";
    this.username = "My Company";

  }
  
  /**
   * @ngdoc method
   * @name HeaderController#_setupWatchers
   * @methodOf app.materialize.header.controller:HeaderController
   * @description sets up the watchers needed to show/hide right links
   */
  _setupWatchers() {
    let $rootScope = ROOTSCOPE.get(this);
    
    $rootScope.$watch(
      () => {
        return AUTHSERVICE.get(this).isAuthorized();
      },
      (nv) => {
        this.isAuthed = nv;
      }
    );
  }
}

module.exports = HeaderController;

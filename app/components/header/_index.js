'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.login
 * @description A module for the login page
 */
module.exports = angular.module('app.components.header', [])
  .controller('HeaderController',
    require('./header.controller'))
  .directive('gcHeader',
    require('./header.directive'));

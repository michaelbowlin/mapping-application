/* global angular */

'use strict';

describe('Unit: Header Component', function () {

  var $compile;
  var ctrl;
  var element;
  var formService;
  var $httpBackend;  
  var localizationService;
  var loginService;
  var $rootScope;
  var $timeout;

  // Mock Localization
  var mockLocalization = {
  };


  beforeEach(function () {
    // Instantiate the app module
    angular.mock.module('app');

    // Store references to $compile and $rootScope so they're available to all
    // tests in this describe block
    // Note that the injector unwraps the underscores from around parameter
    // names when matching
    angular.mock.inject(function (
      _$compile_,
      _$httpBackend_,
      _$q_,
      _$rootScope_) {

      var elm = angular.element('<gc-header></gc-header>');

      $compile = _$compile_;
      $rootScope = _$rootScope_;
      element = $compile(elm)($rootScope);
      $rootScope.$digest();

      ctrl = element.controller('gcHeader');
    });

  });

  it('should exist', function(){
    expect(element).toBeDefined();
  });

  it('should have a container with a page-header class', function() {
    expect( element[0].className.indexOf('page-header') ).toBeGreaterThan(-1);
  });
});
'use strict';

var angular = require('angular');

// Define the list of materialize here
require('./authentication/_index');
require('./companies/_index');
require('./dashboard/_index');
require('./dropdownList/_index');
require('./map/_index');
require('./header/_index');
require('./rsiheader/_index');
require('./leftnav/_index');
require('./footer/_index');
require('./landing/_index');
require('./login/_index');
require('./message/_index');
require('./navbar/_index');
require('./pageHeader/_index');
require('./resetPassword/_index');

// Add materialize here for AngularJS DI bootstrapping
var requires = [
  'app.components.authentication',
  'app.components.companies',
  'app.components.dashboard',
  'app.components.dropdownList',
  'app.components.map',
  'app.components.rsiheader',
  'app.components.header',
  'app.components.leftnav',
  'app.components.footer',
  'app.components.landing',
  'app.components.login',
  'app.components.message',
  'app.components.navbar',
  'app.components.pageHeader',
  'app.components.resetPassword'
];

/**
 * @ngdoc overview
 * @name app.components
 * @description Bootstrapper for the component modules. This is included in
 * application bootstrapping, so there's no need to include this module 
 * directly as part of your DI within the app.
 * 
 * To add a new module (outside of a state - or page - module, such as home,
 * about, etc.), require its bootstrap `_index.js` file in the list of
 * materialize in the `materialize/_index.js` file, then add it to the
 * component DI.
 *
 * @example
 * For example, if you created a new module named `yourModule` in
 * `materialize/yourModule/`, you would add it as a requirement in
 * __`materialize/_index.js`__ like so:
 * <pre>
 * ...
 * require('./yourModule/_index');
 * ...
 * </pre>
 * then, add it to the list of DI requires:
 * <pre>
 * var requires = [
 *   ...
 *   'app.materialize.yourModule'
 * ];
 * </pre>
 */
module.exports = angular.module('app.components', requires);
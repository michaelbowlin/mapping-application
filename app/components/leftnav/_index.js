'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.leftnav
 * @description A module for the leftnav
 */
module.exports = angular.module('app.components.leftnav', [])
  .controller('LeftnavController',
    require('./leftnav.controller'))
  .directive('gcLeftnav',
    require('./leftnav.directive'));

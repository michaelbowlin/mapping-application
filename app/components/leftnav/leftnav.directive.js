'use strict';

var LeftnavController = require('./leftnav.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.leftnav.directive:leftnav
 * @restrict E
 * @description The directive that handles the top leftnav
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-leftnav></data-gc-leftnav>
 * </pre>
 *
 * @ngInject
 */
function gcLeftnav() {

  return {
    bindToController: true,
    controller: LeftnavController,
    controllerAs: 'leftnav',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/leftnav/leftnav.html'
  };

}

module.exports = gcLeftnav;


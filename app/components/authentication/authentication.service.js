'use strict';

const Q = new WeakMap();
const LOCALSTORAGE = new WeakMap();
const RESTANGULAR = new WeakMap();

/**
 * @ngdoc service
 * @name app.materialize.authentication.service:authenticationService
 * @requires $q
 * @requires localStorageService
 * @requires Restangular
 * @description Service for handling API calls for user login
 *
 */
class authenticationService {

  /** @ngInject */
  constructor($q, localStorageService, Restangular) {
    Q.set(this, $q);
    LOCALSTORAGE.set(this, localStorageService);
    RESTANGULAR.set(this, Restangular);

    let service = this;

    /** Public methods */
    service.changePassword = this._changePassword;
    service.getCachedUsername = this._getCachedUsername;
    service.isAuthorized = this._isAuthorized;
    service.login = this._login;
    service.logout = this._logout;
    service.requestResetPassword = this._requestResetPassword;
    service.setCachedUsername = this._setCachedUsername;
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_changePassword
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description calls the API to update user's password
   * @param {String} token the token from the email link
   * @param {String} username the base64 encoded email from the email link
   * @param {String} password the new password for the user
   * @returns {Object} promise object
   */
  _changePassword( token, username, password ) {
    let d = Q.get(this).defer();
    
    if( token && username && password ) {
      let payload = {
        username: username,
        token: token,
        newPassword: password
      };
      
      return RESTANGULAR.get(this)
        .one('resetPassword', 'phase2')
        .customPOST( payload )
        .then(
          () => {
            this._updateSessionStamp();

            d.resolve();
            return d.promise;
          },
          ( error ) => {
            return Q.get(this).reject( error );
          }
        );
    }
    else {
      return Q.get(this).reject({message: 'invalid parameters'});
    }
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_getCachedUsername
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description gets the username from local storage
   * @returns {String} the username or null if none exists
   */
  _getCachedUsername() {
    return LOCALSTORAGE.get(this).get( 'username' );
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_getSessionStamp
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description gets the user's session stamp
   * @returns {Date} session time stamp
   */
  _getSessionStamp() {
    return LOCALSTORAGE.get(this).get('sessionStamp');
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_isAuthorized
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description checks if the user is still authorized
   * @returns {Boolean} true if the user is still authorized
   */
  _isAuthorized() {
    // TODO: flesh out this method
    
    if ( this._getSessionStamp() ) {
      return true;
    }
    
    return false;
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_login
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description logs the user in
   * @returns {Object} $q promise object
   */
  _login( username, password ) {
    let deferred = Q.get( this ).defer();
    
    if ( username && password ) {
      let payload = {
        username: username,
        password: password
      };
      
      return RESTANGULAR.get(this)
        .one('login')
        .customPOST( payload )
        .then(
          () => {
            this._updateSessionStamp();
            
            deferred.resolve();
          },
          ( error ) => {
            // To chain errors in $q you must call $q.reject() and not
            // deferred.reject - if you call the later, the success function
            // will still get called
            return Q.get(this).reject( error );
          } 
        );
    }
    else {
      return Q.get(this).reject( { messages: ['invalid parameters'] } );
    }
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_logout
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description logs out the user and removes the session token
   * from local storage
   * @returns {Object} restangular promise object
   */
  _logout() {
    // first remove session token from local storage
    LOCALSTORAGE.get(this).remove( 'sessionStamp' );
    
    // now call logout API service - no parameters are required
    return RESTANGULAR.get(this)
      .one( 'logout' )
      .customPOST( {} ); 
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_requestResestPassword
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description requests an reset password email to be sent
   * @param {String} username the email address the reset password request
   * is for
   * @returns {Object} restangular promise object
   */
  _requestResetPassword( username ) {
    let params = {
      username: username
    };
    
    return RESTANGULAR.get(this)
      .one( 'resetPassword', 'phase1' )
      .customPOST( params );
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_setCachedUsername
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description sets the username in local storage when remember me is checked
   */
  _setCachedUsername( username ) {
    if ( username ) {
      LOCALSTORAGE.get(this).set( 'username', username );
    }
    else {
      LOCALSTORAGE.get(this).remove( 'username' );
    }
  }
  
  /**
   * @ngdoc method
   * @name authenticationService#_updateSessionTimeout
   * @methodOf app.materialize.authentication.service:authenticationService
   * @description updates the time in local storage for timeout
   */
  _updateSessionStamp() {
    LOCALSTORAGE.get(this).set('sessionStamp', new Date());
  }

}

module.exports = authenticationService;
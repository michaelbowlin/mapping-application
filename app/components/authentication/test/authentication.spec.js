/* global angular */

'use strict';

describe('Unit: Authentication Component', function () {
  
  var $httpBackend;
  var $rootScope;
  var service;
  var localStorage;

  beforeEach(function () {
    // instantiate the app module
    angular.mock.module('app');

    // mock the service
    angular.mock.inject(function(
      _$httpBackend_,
      _$rootScope_,
      _authenticationService_,
      _localStorageService_
    ) {
      
      $httpBackend = _$httpBackend_;
      $rootScope = _$rootScope_;
      service = _authenticationService_;
      localStorage = _localStorageService_;
      
    });
  });

  it('should exist', function () {
    expect(service).toBeDefined();
  });
  
  it('should test the method setCachedUsername', function() {
    service.setCachedUsername( 'test' );
    var user = localStorage.get('username');
    
    expect( user ).toBe( 'test' );
    
    service.setCachedUsername( null );
    user = localStorage.get('username');
    expect(user).toBeNull();
  });
  
  it('should test the method getCachedUsername', function() {
    localStorage.set('username', null);
    expect( service.getCachedUsername() ).toBeNull();
    
    localStorage.set('username', 'getTest');
    expect( service.getCachedUsername() ).toBe( 'getTest' );
  });
  
  it('should test logging in - unhappy path with undefined params', function() {
    
    service.login( ).catch( function(error) {
      expect( error ).toBeDefined();
      expect( error.messages.length ).toBe(1);
    });
    
    $rootScope.$digest();
  });
  
  it('should test logging in - unhappy path unkown user/pw', function() {
    // just testing service, not the login page here so no need
    // for actual error response
    $httpBackend.whenPOST(/login/).respond(401, { message: 'Unauthorized' });
    
    service.login( 'mike@example.com', 'badPW').catch( function( error ) {
      expect( error ).toBeDefined();
    });
    
    $httpBackend.flush();
    $rootScope.$digest();
  });

  it('should test isAuthorized before login', function() {
    expect( service.isAuthorized() ).toBeFalsy();
  });
  
  it('should test logging in - happy path', function() {
    $httpBackend.whenPOST(/login/).respond(200, {});
    
    service.login( 'mike@example.com', 'mike' );
    
    $httpBackend.flush();
    $rootScope.$digest();
    
    expect( localStorage.get( 'sessionStamp' ) ).toBeDefined();
    expect( typeof( localStorage.get( 'sessionStamp' ) ) ).toBe('string');
  });
  
  it('should test isAuthorized after login', function() {
    expect( service.isAuthorized() ).toBeTruthy();
  });
  
  it('should test logout', function() {
    // should still have localStorage value from login happy test
    expect( localStorage.get('sessionStamp') ).toBeDefined();
    
    $httpBackend.whenPOST(/logout/).respond(200, {});
    service.logout();
    $httpBackend.flush();
    $rootScope.$digest();
    
    expect( localStorage.get('sessionStamp') ).toBeNull();
  });
  
  it('should test changePassword - unhappy path: invalid parameters',
    function() {
      service.changePassword( ).catch( function(error) {
        expect( error ).toBeDefined();
        expect( error.message ).toBeDefined();
      });
    
      $rootScope.$digest();
    }
  );
  
  it('should test changePassword - happy path', function() {
    $httpBackend.whenPOST(/phase2/).respond(200, {});
    
    service.changePassword( 'token', 'emailHash', 'Password1' );
    
    $httpBackend.flush();
    $rootScope.$digest();
    
    expect( localStorage.get( 'sessionStamp' ) ).toBeDefined();
  });
  
  it('should test changePassword - link expired', function() {
    $httpBackend.whenPOST(/phase2/).respond(401, { message: 'Unauthorized' });
    
    service.changePassword( 'token', 'emailHash', 'Password1' ).catch( 
      function( error ) {
        expect( error ).toBeDefined();
      }
    );
    
    $httpBackend.flush();
    $rootScope.$digest();
  });
  
});
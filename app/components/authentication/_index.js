'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.authentication
 * @description A module for the authentication service
 */
module.exports = angular.module('app.components.authentication', [])
  .service('authenticationService',
    require('./authentication.service'));
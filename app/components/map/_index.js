'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.map
 * @description A module for the map directive
 */
module.exports = angular.module('app.components.map', [])
  .service('mappingService',
    require('./map.service'))
  .controller('MapController',
    require('./map.controller'))

  .directive('gcMap',
    require('./map.directive'))

  .directive('googlePlaces',
    require('./google-maps.directive'));

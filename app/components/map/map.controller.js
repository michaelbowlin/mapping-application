'use strict';

const MAPPINGSERVICE = new WeakMap(); // get-set.. weak set between Var and outside resource
//const CACHEDSERVICE = new WeakMap();
const DROPDOWNSERVICE = new WeakMap();


/**
 * @ngdoc controller
 * @name app.components.map.controller:MapController
 * @description Controller for the map module.
 *
 */
class MapController {
  /** @ngInject */
  constructor($scope,
              mappingService,
              $location,
              //identityService,
              //mvNotifier,
              //propertyManager,
              $http,
              //cachedDropDownListService
              dropdownListService ) {

    // setup injectibles

    MAPPINGSERVICE.set(this, mappingService);
    //CACHEDSERVICE.set(this, cachedDropDownListService);
    DROPDOWNSERVICE.set(this, dropdownListService);

    /** Set-up the view model */
    let vm = this;
    this._getLists(vm);
    this._getMapData($scope);


    vm.addProperty = function(newProp) {
      var improvementSize, propertyType;
      console.log(newProp);

      if( newProp.improvementSize ){
        improvementSize = newProp.improvementSize;
      } else if( newProp.improvementSizeMulti ){
        improvementSize = newProp.improvementSizeMulti;
      } else if( newProp.landSize ){
        improvementSize = newProp.landSize;
      } else {
        improvementSize = "";
      };

      if( newProp.propertyTypeLand ){
        propertyType = newProp.propertyTypeLand;
      } else if( newProp.propertyTypeIndustrial ){
        propertyType = newProp.propertyTypeIndustrial;
      } else if( newProp.propertyTypeOffice ) {
        propertyType = newProp.propertyTypeOffice;
      } else if( newProp.propertyTypeRetail ) {
        propertyType = newProp.propertyTypeRetail;
      } else if( newProp.propertyTypeMulti ) {
        propertyType = newProp.propertyTypeMulti;
      } else if( newProp.propertyTypeHotel ) {
        propertyType = newProp.propertyTypeHotel;
      } else if( newProp.propertyTypeSpecial ) {
        propertyType = newProp.propertyTypeSpecial;
      } else {
        propertyType = "";
      };

      var newPropertyData = {
        title: newProp.title,
        productType: newProp.productType,
        propertyTypeCategory: newProp.propertyTypeCategory,
        propertyType: propertyType,
        address: newProp.address,
        latCoord: newProp.latCoord,
        longCoord: newProp.longCoord,
        improvementSize: improvementSize,
        relevantCondition: newProp.relevantCondition,
        relevantCondition2: newProp.relevantCondition2,
        relevantCondition3: newProp.relevantCondition3,
        relevantCondition4: newProp.relevantCondition4,
        dateComplete: newProp.dateComplete
        //userAccount: identityService.currentUser._id
      };

      //propertyManager.createProperty(newPropertyData).then(refreshView());

      // Close Lightbox TODO: duplicating close lightbox event. need to combine two
      $('.close-lightbox').on('click', function() {
        $(this).closest('.right-lightbox').removeClass('launch-lightbox');
        $(this).closest('.top-lightbox').removeClass('launch-lightbox');
        if (!$('.right-lightbox').hasClass('launch-lightbox')) {
          setTimeout(function() {
            $('.lightbox-container').removeClass('launch-lightbox');
          }, 200);
        }
      });
    }

    function refreshView() {

      $scope.refreshMap()();
      $scope.refreshGrid()();
    }

  } //. --------------------------------------- end Constructor


  _getLists(vm) {

    DROPDOWNSERVICE.get(this).getDropdownLists().then(function(response){

      vm.ddlStates = response["State"].list;
      vm.ddlProductType = response["Product Type"].list;
      vm.ddlPropertyTypeCategory = response["Property Type: Categories"].list;
      vm.ddlPropertyTypeLand = response["Property Type: Land"].list;
      vm.ddlPropertyTypeIndustrial = response["Property Type: Industrial"].list;
      vm.ddlPropertyTypeOffice = response["Property Type: Office"].list;
      vm.ddlPropertyTypeRetail = response["Property Type: Retail"].list;
      vm.ddlPropertyTypeMultiFamily = response["Property Type: Multi-Family"].list;
      vm.ddlPropertyTypeHotel = response["Property Type: Hotel"].list;
      vm.ddlPropertyTypeSpecialPurpose = response["Property Type: Special Purpose"].list;
      vm.ddlImprovementSize = response["Improvement Size"].list;
      vm.ddlImprovementSizeMultiFamily = response["Improvement Size: Multi-Family"].list;
      vm.ddlLandSize = response["Land Size"].list;
      vm.ddlRelevantCondition = response["Relevant Condition"].list;
    });
  }

  _getMapData($scope) {
    MAPPINGSERVICE.get(this).getMapPoints().then(function (data) {
      if (data && data.length) {

        //console.log(data);

        //_createMap(data);
        var mapOptions = {
          zoom: 4,
          center: new google.maps.LatLng(40.0000, -98.0000),
          mapTypeId: google.maps.MapTypeId.TERRAIN
        }

        $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var infoWindow = new google.maps.InfoWindow();

        var cities = data;


        for (var i = 0; i < cities.length; i++) {
          //_createMarker(cities[i]);
          createMarker(cities[i]);
        }

        function createMarker(info) {

          var marker = new google.maps.Marker({
            map: $scope.map,
            position: new google.maps.LatLng(info.latCoord, info.longCoord),
            title: info.city
          });

          marker.content = '<div class="infoWindowContent">' + info.description + '</div>';
          var infoWindow = new google.maps.InfoWindow();

          google.maps.event.addListener(marker, 'click', function () {
            infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
            infoWindow.open($scope.map, marker);
          });

          var markers = [];

          markers.push(marker);
        }


      }
    });
  }

}




module.exports = MapController;


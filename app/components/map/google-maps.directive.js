'use strict';

var MapController = require('./map.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.pageHeader.directive:googlePlaces
 * @restrict E
 * @description The directive that handles the googlePlaces autocomplete
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-google-places></data-google-places>
 * </pre>
 *
 * @ngInject
 */
function googlePlaces() {



    //return {
    //    bindToController: true,
    //    controller: MapController,
    //    controllerAs: 'property',
    //    replace: true,
    //    restrict: 'A',
    //    require: '?ngModel',
    //    link: function(scope, element, attrs, ngModel, $scope, $log) {
    //        var autocomplete = new google.maps.places.Autocomplete(element[0]);
    //        google.maps.event.addListener(autocomplete, 'place_changed', function() {
    //            var address = element.prop('value')
    //            ngModel.$setViewValue(address);
    //        });
    //    }
    //};




    return {
        controller: MapController,
        controllerAs: 'property',
        restrict: 'E',
        scope: {
            address: '=',
            lat: '=',
            lon: '='
        },

        //templateUrl: 'components/map/google-maps.html',

        link: function($scope, elm, attrs){
            var autocomplete = new google.maps.places.Autocomplete($("#google_places_ac")[0], {});
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                $scope.location = place.geometry.location.lat() + ',' + place.geometry.location.lng();


                if ($scope.location === '') {
                    alert('Directive did not update the location property in parent controller.');
                } else {
                    var fullAddress =  place.formatted_address;
                    var latLon = $scope.location;
                    var res = latLon.split(",");
                    var latitude = res[0];
                    var longitude = res[1];

                    // Put lat/lon/address into scope
                    $scope.lat = latitude;
                    $scope.lon = longitude;
                    $scope.address = fullAddress;

                }

                $scope.$apply();


            });
        }
    };

}

module.exports = googlePlaces;

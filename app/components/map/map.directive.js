'use strict';

var MapController = require('./map.controller.js');

/**
 * @ngdoc directive
 * @name app.components.map.directive:dashboard
 * @restrict E
 * @description The directive that handles the map
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-map></data-gc-map>
 * </pre>
 *
 * @ngInject
 */
function gcMap() {

  return {
    bindToController: true,
    controller: MapController,
    controllerAs: 'property',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/map/map.html'
  };

}

module.exports = gcMap;


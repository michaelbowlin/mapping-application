'use strict';

const Q = new WeakMap();
//const LOCALSTORAGE = new WeakMap();
const RESTANGULAR = new WeakMap();

/**
 * @ngdoc service
 * @name app.materialize.authentication.service:mappingService
 * @requires $q
 * @requires localStorageService
 * @requires Restangular
 * @description Service for handling API calls for user login
 *
 */
class mappingService {

    /** @ngInject */
    constructor($q, Restangular) {
        Q.set(this, $q);
        //LOCALSTORAGE.set(this, localStorageService);
        RESTANGULAR.set(this, Restangular);

        let service = this;

        /** Public methods */
        service.getMapPoints = this._getMapPoints;
        //service.changePassword = this._changePassword;
        //service.getCachedUsername = this._getCachedUsername;
        //service.isAuthorized = this._isAuthorized;
        //service.login = this._login;
        //service.logout = this._logout;
        //service.requestResetPassword = this._requestResetPassword;
        //service.setCachedUsername = this._setCachedUsername;
    }

    /**
     * @ngdoc method
     * @name mappingService#_changePassword
     * @methodOf app.materialize.authentication.service:authenticationService
     * @description calls the API to update user's password
     * @param {String} token the token from the email link
     * @param {String} username the base64 encoded email from the email link
     * @param {String} password the new password for the user
     * @returns {Object} promise object
     */
    _getMapPoints( ) {
        return RESTANGULAR.get(this)
            .one('properties')
            .get ()

    }

}

module.exports = mappingService;
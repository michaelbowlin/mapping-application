'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.landing
 * @description Module for the landing page
 */
module.exports = angular.module('app.components.landing', [])
  .controller('LandingController',
    require('./landing.controller'))
  .directive('gcLanding',
    require('./landing.directive'));

'use strict';

var LandingController = require('./landing.controller');

/**
 * @ngdoc directive
 * @name app.materialize.landing.directive:landing
 * @restrict E
 * @description The directive that produces the landing state output
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-landing></data-gc-landing>
 * </pre>
 * 
 * @ngInject
 */
function gcLanding() {

  return {
    bindToController: true,
    controller: LandingController,
    controllerAs: 'landing',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/landing/landing.html'
  };
  
}

module.exports = gcLanding;
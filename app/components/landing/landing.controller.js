'use strict';

/**
 * @ngdoc controller
 * @name  app.materialize.landing.controller:LandingController
 * @requires $state
 * @description Controller for the landing page directive.
 * 
 */
class LandingController {
  /** @ngInject */
  constructor( $state ) {
    // Just redirect to login page - for now
    
    $state.go( 'LOGIN' );
  }
  
}

module.exports = LandingController;

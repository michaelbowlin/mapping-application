/* global angular */

'use strict';

describe('Unit: Landing Component', function() {

  var ctrl;
  var $compile;
  var $rootScope;
  var $state;
  var service;

  beforeEach(function() {
    // instantiate the app module
    angular.mock.module('app');

    // mock the controller
    angular.mock.inject(
      function(
        _$compile_, 
        _$rootScope_, 
        _$state_
      ) {
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $state = _$state_;
      }
    );
  });

  it('should instatiate the directive/controller',
    function () {
      
      spyOn( $state, 'go' );
      // Compile a piece of HTML containing the directive
      var element = $compile('<gc-landing></gc-landing>')($rootScope);

      // Fire off all watches
      $rootScope.$digest();
      
      expect( $state.go ).toHaveBeenCalled();
    }
  );
});
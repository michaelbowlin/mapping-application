'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.footer
 * @description A module for the footer directive
 */
module.exports = angular.module('app.components.footer', [])
  .controller('FooterController',
    require('./footer.controller'))
  .directive('gcFooter',
    require('./footer.directive'));

'use strict';

const AUTHSERVICE = new WeakMap();
const ROOTSCOPE = new WeakMap();

/**
 * @ngdoc controller
 * @name  app.materialize.footer.controller:FooterController
 * @requires $rootScope
 * @requires app.materialize.authentication.service:authenticationService
 * @description Controller for the footer module.
 *
 */
class FooterController {
  /** @ngInject */
  constructor( $rootScope, authenticationService ) {

    // setup injectibles
    ROOTSCOPE.set(this, $rootScope);
    AUTHSERVICE.set(this, authenticationService);

    /** Set-up the view model */
    let vm = this;

    // properties
    vm.isAuthed = false; // default to hidden

    this._setupLocalization();
    this._setupWatchers();
  }

  /**
   * @ngdoc method
   * @name FooterController#_setupLocalization
   * @methodOf app.materialize.footer.controller:FooterController
   * @description sets up the localization strings for the footer
   */
  _setupLocalization() {
    this.copyrightDate = new Date();

    // TODO: add localization service call when it's ready
    this.copyrightText = "Company Name";
    this.contactSupport = "Contact Support";
    this.privacyPolicy = "Privacy Policy";
    this.termsOfUse = "Terms & Conditions";
  }

  /**
   * @ngdoc method
   * @name FooterController#_setupWatchers
   * @methodOf app.materialize.footer.controller:FooterController
   * @description sets up the watchers needed to show/hide right links
   */
  _setupWatchers() {
    let $rootScope = ROOTSCOPE.get(this);

    $rootScope.$watch(
      () => {
        return AUTHSERVICE.get(this).isAuthorized();
      }, (nv) => {
        this.isAuthed = nv;
      }
    );
  }
}

module.exports = FooterController;

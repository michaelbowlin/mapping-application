'use strict';

var FooterController = require('./footer.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.footer.directive:footer
 * @restrict E
 * @description The directive that handles the footer
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-footer></data-gc-footer>
 * </pre>
 *
 * @ngInject
 */
function gcFooter() {

  return {
    bindToController: true,
    controller: FooterController,
    controllerAs: 'footer',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/footer/footer.html'
  };

}

module.exports = gcFooter;


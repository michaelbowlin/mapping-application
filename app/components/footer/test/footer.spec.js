/* global angular */

'use strict';

describe('Unit: Footer Component', function () {

  var $compile;
  var ctrl;
  var element;
  var $rootScope;

  // Mock Localization
  var mockLocalization = {
  };


  beforeEach(function () {
    // Instantiate the app module
    angular.mock.module('app');

    // Store references to $compile and $rootScope so they're available to all
    // tests in this describe block
    // Note that the injector unwraps the underscores from around parameter
    // names when matching
    angular.mock.inject(function (
      _$compile_,
      _$rootScope_) {

      var elm = angular.element('<gc-footer></gc-footer>');
      var vm = this;

      $compile = _$compile_;
      $rootScope = _$rootScope_;
      element = $compile(elm)($rootScope);
      $rootScope.$digest();

      ctrl = element.controller('gcFooter');
    });

  });

  it('should exist', function(){
    expect(element).toBeDefined();
  });

  it('should have a footer element', function() {
    expect( element[0].tagName ).toBe('FOOTER');
  });

  it('should have a link to Contact Support when logged out', function(){
    ctrl.isAuthed = false;
    $rootScope.$digest();

    var link = element[0].querySelectorAll('a')[0];
    expect( link.innerText ).toEqual('Contact Support');
  });

  it('should have show the company logo when logged in', function(){

    ctrl.isAuthed = true;
    $rootScope.$digest();

    var link = element[0].querySelectorAll('img')[0];
    expect( link ).toBeDefined();
  });

  it('should have a link to Privacy Policy when logged in', function(){
    ctrl.isAuthed = true;
    $rootScope.$digest();

    var link = element[0].querySelectorAll('a')[0];
    expect( link.innerText ).toEqual('Privacy Policy');
  });

  it('should have a link to Terms & Conditions when logged in', function(){
    ctrl.isAuthed = true;
    $rootScope.$digest();

    var link = element[0].querySelectorAll('a')[1];
    expect( link.innerText ).toEqual('Terms & Conditions');
  });
  
});
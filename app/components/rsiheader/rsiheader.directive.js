'use strict';

var RsiheaderController = require('./rsiheader.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.rsiheader.directive:rsiheader
 * @restrict E
 * @description The directive that handles the top rsiheader
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-rsiheader></data-gc-rsiheader>
 * </pre>
 *
 * @ngInject
 */
function gcRsiheader() {

  return {
    bindToController: true,
    controller: RsiheaderController,
    controllerAs: 'rsiheader',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/rsiheader/rsiheader.html'
  };

}

module.exports = gcRsiheader;


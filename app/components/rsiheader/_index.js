'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.login
 * @description A module for the login page
 */
module.exports = angular.module('app.components.rsiheader', [])
  .controller('RsiheaderController',
    require('./rsiheader.controller'))
  .directive('gcRsiheader',
    require('./rsiheader.directive'));

'use strict';

var PageHeaderController = require('./pageHeader.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.pageHeader.directive:pageHeader
 * @restrict E
 * @description The directive that handles the header and search box
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-page-header></data-gc-page-header>
 * </pre>
 *
 * @ngInject
 */
function gcPageHeader() {

  return {
    bindToController: true,
    controller: PageHeaderController,
    controllerAs: 'header',
    replace: true,
    restrict: 'E',
    scope: {
      createText: '@',
      pageTitle: '@',
      searchPlaceholder: '@',
      showSearch: '='
    },
    templateUrl: 'components/pageHeader/pageHeader.html'
  };

}

module.exports = gcPageHeader;


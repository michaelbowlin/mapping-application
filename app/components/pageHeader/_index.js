'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.pageHeader
 * @description A module for the page header that includes page title and search 
 * box
 */
module.exports = angular.module('app.components.pageHeader', [])
  .controller('PageHeaderController',
    require('./pageHeader.controller'))
  .directive('gcPageHeader',
    require('./pageHeader.directive'));

'use strict';

const STATE = new WeakMap();
/**
 * @ngdoc controller
 * @name  app.materialize.pageHeader.controller:PageHeaderController
 * @description Controller for the page header module.
 *
 */
class PageHeaderController {
  /** @ngInject */
  constructor($state) {

    STATE.set(this, $state);
    /** Set-up the view model */
    let vm = this;

    this.goToCreate = this._goToCreate;
  }

   /**
   * @ngdoc method
   * @name PageHeaderController#_goToCreate
   * @methodOf app.materialize.pageHeader.controller:PageHeaderController
   * @description handles the redirect to the sub-state create
   */
  
  _goToCreate() {

    let state = STATE.get(this);
    state.go(state.current.parent + '.CREATE');

  }
  
}

module.exports = PageHeaderController;
'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.dropdownlist
 * @description A module for the dropdownlist directive
 */
module.exports = angular.module('app.components.dropdownList', [])
    .service('dropdownListService',
    require('./dropdownList.service'));

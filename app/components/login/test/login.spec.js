/* global angular */

'use strict';

describe('Unit: Login Component', function () {

  var $compile;
  var $q;
  var $state;
  var ctrl;
  var element;
  var $httpBackend;  
  var authService;
  var $rootScope;
  var localStorage;

  // Mock Localization
  var mockLocalization = {
  };


  beforeEach(function () {
    // Instantiate the app module
    angular.mock.module('app');

    // Store references to $compile and $rootScope so they're available to all
    // tests in this describe block
    // Note that the injector unwraps the underscores from around parameter
    // names when matching
    angular.mock.inject(function (
      _$compile_,
      _$httpBackend_,
      _$q_,
      _$state_,
      _$rootScope_,
      _authenticationService_,
      _localStorageService_
    ) {
      
      authService = _authenticationService_;
      localStorage = _localStorageService_;
      $q = _$q_;
      $state = _$state_;

      var elm = angular.element('<gc-login></gc-login>');

      $compile = _$compile_;
      $rootScope = _$rootScope_;
      element = $compile(elm)($rootScope);
      $rootScope.$digest();

      ctrl = element.controller('gcLogin');
    });

  });

  it('should exist', function(){
    expect(element).toBeDefined();
  });

  it('should have a container with id: login', function() {
    expect( element[0].id ).toBe('login');
  });
  
  it('should have a defined login service', function() {
    expect( authService ).toBeDefined();
  });
  
  it('should test the load user method in the controller', function() {
    localStorage.set('username', 'test@example.com');
    
    ctrl._loadUser();
    
    $rootScope.$digest();
    
    var inp = element[0].querySelector('input[name="username"]');
    expect( inp.value ).toBe('test@example.com');
  });
  
  it('should test handleLogin - happy path', function() {
    ctrl.username = 'test@example.com';
    ctrl.password = 'testpw';
    ctrl.rememberMe = true;
    ctrl.loginForm.username.$setDirty( true );
    ctrl.loginForm.password.$setDirty( true );
    
    $rootScope.$digest();
    
    spyOn( authService, 'setCachedUsername' );
    
    spyOn( authService, 'login' ).andCallFake( function() {
      var d = $q.defer();
      d.resolve({});
      return d.promise;
    } );
    
    spyOn( $state, 'go' );
    
    ctrl.handleLogin();
    $rootScope.$digest();
    
    expect( authService.setCachedUsername ).toHaveBeenCalled();
    expect( authService.login ).toHaveBeenCalled();
    expect( $state.go ).toHaveBeenCalled();
  });
  
  it('should test handleLogin - unhappy path: invalid user', function() {
    ctrl.username = 'test@example.com';
    ctrl.password = 'testpw';
    ctrl.loginForm.username.$setDirty( true );
    ctrl.loginForm.password.$setDirty( true );
    
    $rootScope.$digest();
    
    spyOn( authService, 'login' ).andCallFake( function() {
      var d = $q.defer();
      d.reject({ status: 401});
      return d.promise;
    } );
    
    ctrl.handleLogin();
    $rootScope.$digest();
    
    expect( ctrl.showUnauthed ).toBeTruthy();
  });
  
  it('should test handleLogin - unhappy path: locked user', function() {
    ctrl.username = 'test@example.com';
    ctrl.password = 'testpw';
    ctrl.loginForm.username.$setDirty( true );
    ctrl.loginForm.password.$setDirty( true );
    
    $rootScope.$digest();
    
    spyOn( authService, 'login' ).andCallFake( function() {
      var d = $q.defer();
      d.reject( {
        status: 401, 
        data: {
          errorDetail: 'GCAccountLockedException'
        }
      } );
      return d.promise;
    } );
    
    ctrl.handleLogin();
    $rootScope.$digest();
    
    expect( ctrl.accountLocked ).toBeTruthy();
  });
  
  it('should test handleLogin - unhappy path: form invalid', function() {
    
    ctrl.handleLogin();
    $rootScope.$digest();
    
    expect( ctrl.showValidation ).toBeTruthy();
  });
  
});
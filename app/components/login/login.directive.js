'use strict';

var LoginController = require('./login.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.login.directive:login
 * @restrict E
 * @description The directive used for logging in
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-login></data-gc-login>
 * </pre>
 *
 * @ngInject
 */
function gcLogin() {

  return {
    bindToController: true,
    controller: LoginController,
    controllerAs: 'login',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/login/login.html'
  };

}

module.exports = gcLogin;


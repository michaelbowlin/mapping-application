'use strict';

const AUTHSERVICE = new WeakMap();
const STATE = new WeakMap();

/**
 * @ngdoc controller
 * @name  app.materialize.login.controller:LoginController
 * @requires $state
 * @requires app.materialize.authentication.service:authenticationService
 * @description Controller for the login module. Sets-up everything the view
 * needs to render the login directive.
 *
 */
class LoginController {
  /** @ngInject */
  constructor($state, authenticationService) {
    
    // setup injectibles
    STATE.set(this, $state);
    AUTHSERVICE.set(this, authenticationService);
    
    /** Set-up the view model */
    let vm = this;
    
    // Public Methods
    vm.handleLogin = this._handleLogin;
    
    // Public Properties
    vm.accountLocked = false;
    vm.showUnauthed = false;
    vm.showValidation = false;
    
    this._setupLocalization();
    this._loadUser();
  }
  
  /**
   * @ngdoc method
   * @name LoginController#_handleLogin
   * @methodOf app.materialize.login.controller:LoginController
   * @description handles the login button click
   */
  _handleLogin() {
    let authSvc = AUTHSERVICE.get(this);

    if ( this.loginForm.$dirty && this.loginForm.$valid ) {
      
      authSvc.setCachedUsername( this.rememberMe ? this.username : null );
      
      authSvc.login( this.username, this.password ).then(
        () => {
          STATE.get(this).go( 'DASHBOARD', { reload: true, inherit: false } );
        },
        ( error ) => {
          var errorData = error && error.data ? error.data : null;
          if ( errorData &&
            errorData.errorDetail === 'GCAccountLockedException' ) {
            
            this.accountLocked = true;
          }
          else {
            this.showUnauthed = true;
          }
        }
      );
    }
    else {
      this.showValidation = true;
    }
  }
  
  /**
   * @ngdoc method
   * @name LoginController#_loadUser
   * @methodOf app.materialize.login.controller:LoginController
   * @description grabs the username from local storage
   * if Remember Me was checked during login
   */
  _loadUser() {
    let user = AUTHSERVICE.get(this).getCachedUsername();
    if ( user ) {
      this.username = user;
      this.rememberMe = true;
    }
  }
  
  /**
   * @ngdoc method
   * @name LoginController#_setupLocalization
   * @methodOf app.materialize.login.controller:LoginController
   * @description sets up the localization strings for the login form
   */
  _setupLocalization() {
    // TODO: add localization service call when it's ready
    
    this.heading = 'Login';
    this.emailLabel = 'Email';
    this.rememberMeLabel = 'Remember Me';
    this.passwordLabel = 'Password';
    this.resetPasswordLabel = 'Reset Password';
    this.loginButtonLabel = 'Login';
    
    // error messaging
    this.unrecognizedTitle = "Sorry...";
    this.unrecognizedBody = "We do not recognize your email and/or password.";
    this.emailRequired = "Email address is required";
    this.emailFormat = "Email address not in correct format";
    this.passwordRequired = "Password is required";
    
    this.lockedTitle = "Sorry...";
    this.lockedError = "You have reached the maximum number of login attempts.";
    this.lockedContact = "Please speak with a Administrator before attempting to login again.";
    this.lockedContactLink = "Please contact us.";
  }
  
}

module.exports = LoginController;
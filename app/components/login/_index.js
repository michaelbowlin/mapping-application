'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.login
 * @description A module for the login page
 */
module.exports = angular.module('app.components.login', [])
  .controller('LoginController',
    require('./login.controller'))
  .directive('gcLogin',
    require('./login.directive'));

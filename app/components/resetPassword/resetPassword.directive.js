'use strict';

var ResetPasswordController = require('./resetPassword.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.resetPassword.directive:resetPassword
 * @restrict E
 * @description The directive used for resetting a password
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-reset-password></data-gc-reset-password>
 * </pre>
 *
 * @ngInject
 */
function gcResetPassword() {

  return {
    bindToController: true,
    controller: ResetPasswordController,
    controllerAs: 'resetPassword',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/resetPassword/resetPassword.html'
  };

}

module.exports = gcResetPassword;


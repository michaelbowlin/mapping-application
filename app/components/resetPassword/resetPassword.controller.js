'use strict';

const AUTHSERVICE = new WeakMap();
const NOTIFICATIONSVC = new WeakMap();
const STATE = new WeakMap();
const STATEPARAMS = new WeakMap();

/**
 * @ngdoc controller
 * @name  app.materialize.resetPassword.controller:ResetPasswordController
 * @requires $stateParams
 * @requires app.materialize.authentication.service:authenicationService
 * @requires gc.ui.materialize.notification.service:notificationService
 * @description Controller for the reset password module. Sets-up everything
 * the view needs to render the reset password directive.
 *
 */
class ResetPasswordController {
  /** @ngInject */
  constructor(
    $state,
    $stateParams,
    authenticationService,
    notificationService
  ) {
    
    // setup injectibles
    AUTHSERVICE.set(this, authenticationService);
    NOTIFICATIONSVC.set(this, notificationService);
    STATE.set(this, $state);
    STATEPARAMS.set(this, $stateParams);
    
    /** Set-up the view model */
    let vm = this;
    vm.getValidPasswordClass = this._getValidPasswordClass;
    vm.getValidPassword2Class = this._getValidPassword2Class;
    vm.getValidUsernameClass = this._getValidUsernameClass;
    vm.submitCreatePassword = this._submitCreatePassword;
    vm.submitResetRequest = this._submitResetRequest;
    
    // Properties
    vm.pageState = 'request';
    vm.showValidation = false;
    
    if ( $stateParams.token && $stateParams.email ) {
      vm.pageState = 'update';
    }
    
    // TODO: Expired link
    
    this._setupLocalization();
  }
  
  /**
   * @ngdoc method
   * @name ResetPasswordController#_getPasswordClass
   * @methodOf app.materialize.resetPassword.controller:ResetPasswordController
   * @description helper method for getting icon class for a password field
   * @returns {String} the class name for valid/invalid or empty string
   */
  _getPasswordClass( formElement ) {
    if ( formElement.$touched ) {
      return formElement.$valid ? 'fa-check-circle' : 'fa-exclamation-circle';
    }
    
    return '';
  }
  
  /**
   * @ngdoc method
   * @name ResetPasswordController#_getValidPasswordClass
   * @methodOf app.materialize.resetPassword.controller:ResetPasswordController
   * @description gets the icon class for the password field
   * @returns {String} the class name for valid/invalid or empty string
   */
  _getValidPasswordClass() {
    return this._getPasswordClass( this.resetPasswordForm.password );
  }
  
  /**
   * @ngdoc method
   * @name ResetPasswordController#_getValidPassword2Class
   * @methodOf app.materialize.resetPassword.controller:ResetPasswordController
   * @description gets the icon class for the password2 field
   * @returns {String} the class name for valid/invalid or empty string
   */
  _getValidPassword2Class() {
    return this._getPasswordClass( this.resetPasswordForm.password2 );
  }
  
  /**
   * @ngdoc method
   * @name ResetPasswordController#_getValidUsernameClass
   * @methodOf app.materialize.resetPassword.controller:ResetPasswordController
   * @description gets the class for the icon next to the username form field
   * @returns {String} the class name for valid/invalid or empty string
   */
  _getValidUsernameClass() {
    let form = this.resetPasswordForm;
    let frmUsername = form.username;
    
    if (frmUsername.$touched && ( frmUsername.$error.email ||
      frmUsername.$error.required ) ) {
      return 'fa-exclamation-circle';
    }
    else if (this.username && frmUsername.$touched && frmUsername.$valid) {
      return 'fa-check-circle';
    }
    
    return '';
  }
  
  /**
   * @ngdoc method
   * @name ResetPasswordController#_setupLocalization
   * @methodOf app.materialize.resetPassword.controller:ResetPasswordController
   * @description sets up the localization strings for the reset password form
   */
  _setupLocalization() {
    // TODO: add localization service call when it's ready
    
    // Request password reset
    this.heading = 'Reset Your Password';
    this.resetInfoText = 'To reset your password, begin by entering the email address you use to access the system.';
    this.emailLabel = 'Email';
    this.submitButtonLabel = 'Submit';
    
    // Change Password
    this.createNewPasswordTitle = 'Create New Password';
    this.createNewPasswordInfo = 'Your password must be at least 8 characters in length, include at least 1 capital letter, 1 lowercase letter, and 1 number';
    this.passwordLabel = 'Enter New Password';
    this.reEnterPasswordLabel = 'Re-Enter New Password';
    
    // Link Expired
    this.oops='Ooops...';
    this.linkExpired='This link is expired. Please request a new password reset link.';
    this.resetPassword='Reset Password';
    
    // Success
    this.successTitle = 'Success!';
    this.successInfoSent = 'An email is being sent to you.  For your security, the reset email is only active for the next 24 hours.';
    this.successInfoSpam = "If you don't see the email in the next 10 minutes, check your spam folder first, then try sending it again";
    this.successInfoDontSee = "Still don't see it?";
    this.contactUs = 'Please contact us.';
    
    // Errors
    this.invalidEmail = 'Invalid email format';
    this.usernameRequired = "Username is required";
    this.passwordRequired = 'Password is required';
    this.passwordInvalid = 'Password requirements not met';
    this.passwordMatch = 'Passwords do not match';
  }
  
  /**
   * @ngdoc method
   * @name ResetPasswordController#_submitCreatePassword
   * @methodOf app.materialize.resetPassword.controller:ResetPasswordController
   * @description sets up the data needed to update the password for the user
   * and calls the authentication service.  Handles any errors coming back
   * from the API
   */
  _submitCreatePassword() {
    if ( this.resetPasswordForm.$valid && this.resetPasswordForm.$dirty ) {
      let $stateParams = STATEPARAMS.get(this);
      AUTHSERVICE.get(this)
        .changePassword( $stateParams.token, $stateParams.email, this.password )
        .then(
          () => {
            NOTIFICATIONSVC.get(this).notify(
              'success',
              'Success!',
              'Your password was reset.'
            );
            STATE.get(this).go( 'DASHBOARD', { reload: true, inherit: false } );
          },
          (error) => {
            // display error message
            if (error && 
              error.data && 
              error.data.errorDetail === "GCTokenExpiredException" ) {
              
              this.pageState = 'expired';
            }
            else {
              // we have some other error
              // TODO: what do we do in this case?
              // for now go to error page
              STATE.get(this).go('MESSAGE', { errorCode: 'SERVER_ERROR' } );
            }
          }
        );
    }
    else {
      this.showValidation = true;
    }
  }
  
  /**
   * @ngdoc method
   * @name ResetPasswordController#_submitResetRequest
   * @methodOf app.materialize.resetPassword.controller:ResetPasswordController
   * @description sets up the form data to make the call to the reset password
   * method of the login service.  Also handles if there are any errors returned
   * from the service call
   */
  _submitResetRequest() {
    if ( this.resetPasswordForm.$valid && this.resetPasswordForm.$dirty ) {
      AUTHSERVICE.get(this).requestResetPassword( this.username ).then(
        () => {
          this.pageState = 'success';
        },
        () => {
          // for now we're swallowing the error for security reasons
          // (i.e, if the email doesn't exist in the system, don't
          // tell the user)
          this.pageState = 'success';
        }
      );
    }
    else {
      this.showValidation = true;
    }
  }

}

module.exports = ResetPasswordController;
'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.resetPassword
 * @description A module for the reset password page
 */
module.exports = angular.module('app.components.resetPassword', [])
  .controller('ResetPasswordController',
    require('./resetPassword.controller'))
  .directive('gcResetPassword',
    require('./resetPassword.directive'));

/* global angular */

'use strict';

describe('Unit: Reset Password Component', function () {

  var $compile;
  var ctrl;
  var element;
  var $httpBackend;
  var $q;
  var $rootScope;
  var $state;
  
  var authService;

  // Mock Localization
  var mockLocalization = {
  };

  describe( 'Reset Password Request', function() {
    
    beforeEach(function () {
      // Instantiate the app module
      angular.mock.module('app');

      // Store references to $compile and $rootScope so they're available to all
      // tests in this describe block
      // Note that the injector unwraps the underscores from around parameter
      // names when matching
      angular.mock.inject(function (
        _$compile_,
        _$httpBackend_,
        _$q_,
        _$rootScope_,
        _$state_,
        _authenticationService_
      ) {
        
        $compile = _$compile_;
        $httpBackend = _$httpBackend_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $state = _$state_;
        authService = _authenticationService_;
        
        var elm = angular.element('<gc-reset-password></gc-reset-password>');

        element = $compile(elm)($rootScope);
        $rootScope.$digest();
      
        ctrl = element.controller('gcResetPassword');
      });

    });

    it('should exist', function(){
      expect(element).toBeDefined();
    });

    it('should have a container with id: reset-password', function() {
      expect( element[0].id ).toBe('reset-password');
    });
    
    it('should test submitting the blank form', function() {
      ctrl.submitResetRequest();
      $rootScope.$digest();
      
      expect(ctrl.showValidation).toBeTruthy();
      
      var error = element[0].querySelectorAll('p.error')[1];
      
      expect(angular.element( error ).hasClass('ng-hide')).toBeFalsy();
    });
  
    it('should test the success state', function() {
      
      $httpBackend.whenPOST(/phase1/).respond( 200, {});
      
      ctrl.username = 'test@example.com';
      ctrl.resetPasswordForm.$setDirty();
      $rootScope.$digest();
      ctrl.submitResetRequest();
      $httpBackend.flush();
      $rootScope.$digest();
    
      var heading = element[0].querySelector('h2');
      expect( heading ).toBeDefined();
      expect( heading.innerText ).toBe('Success!');
    });
    
    it('should test the failure state', function() {
      
      $httpBackend.whenPOST(/phase1/).respond( 401, {});
      
      // note this will still go to success page
      ctrl.username = 'test@example.com';
      ctrl.resetPasswordForm.$setDirty();
      $rootScope.$digest();
      ctrl.submitResetRequest();
      $httpBackend.flush();
      $rootScope.$digest();
    
      var heading = element[0].querySelector('h2');
      expect( heading ).toBeDefined();
      expect( heading.innerText ).toBe('Success!');
    });
    
    it('should test the error icon', function() {
      ctrl.resetPasswordForm.username.$setTouched();
      ctrl.username = 'test';
      $rootScope.$digest();
      
      var icon = angular.element(element[0].querySelector('i'));
      
      expect(icon.hasClass('fa-exclamation-circle')).toBeTruthy();
    });
    
    it('should test the valid icon', function() {
      ctrl.resetPasswordForm.username.$setTouched();
      ctrl.username = 'test@example.com';
      $rootScope.$digest();
      
      var icon = angular.element(element[0].querySelector('i'));
      
      expect(icon.hasClass('fa-check-circle')).toBeTruthy();
    });
  });
  
  describe( 'Create Password', function() {
    
    beforeEach(function () {
      // Instantiate the app module
      angular.mock.module('app');

      // Store references to $compile and $rootScope so they're available to all
      // tests in this describe block
      // Note that the injector unwraps the underscores from around parameter
      // names when matching
      angular.mock.inject(function (
        _$compile_,
        _$httpBackend_,
        _$q_,
        _$rootScope_,
        _$state_,
        _authenticationService_
      ) {

        $compile = _$compile_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $state = _$state_;
        authService = _authenticationService_;
        
        var elm = angular.element('<gc-reset-password></gc-reset-password>');
        element = $compile(elm)($rootScope);
        $rootScope.$digest();
      
        ctrl = element.controller('gcResetPassword');
        ctrl.pageState = 'update';
        $rootScope.$digest();
      });

    });
    
    it('should test the create password state', function() {
      var header = element[0].querySelector('h2');
      
      expect(header.innerText).toBe('Create New Password');
    });
    
    it('should test valid submission', function() {
      
      // mock the service call, we're testing the service in it's spec
      spyOn( authService, 'changePassword' ).andCallFake( function() {
        var d = $q.defer();
        d.resolve();
        return d.promise;
      });
      
      spyOn( $state, 'go' );
      
      ctrl.resetPasswordForm.password.$setDirty();
      ctrl.resetPasswordForm.password2.$setDirty();
      
      ctrl.password = 'Password1';
      ctrl.password2 = 'Password1';
      
      $rootScope.$digest();
      
      ctrl.submitCreatePassword();
      $rootScope.$digest();
      
      expect( authService.changePassword ).toHaveBeenCalled();
      expect( $state.go ).toHaveBeenCalled();
    });
    
    it('should test the expired link state', function() {
      // mock the service call, we're testing the service in it's spec
      spyOn( authService, 'changePassword' ).andCallFake( function() {
        var d = $q.defer();
        d.reject({
          status: 401,
          data: {
            errorDetail: 'GCTokenExpiredException'
          }
        });
        return d.promise;
      });
      
      spyOn( $state, 'go' );
      
      ctrl.resetPasswordForm.password.$setDirty();
      ctrl.resetPasswordForm.password2.$setDirty();
      
      ctrl.password = 'Password1';
      ctrl.password2 = 'Password1';
      
      $rootScope.$digest();
      
      ctrl.submitCreatePassword();
      $rootScope.$digest();
      
      expect( authService.changePassword ).toHaveBeenCalled();
      expect( ctrl.pageState ).toBe('expired');
    });
    
  });
});
/* global angular */

'use strict';

describe('Unit: Dashboard Component', function () {

  var $compile;
  var ctrl;
  var element;
  var $rootScope;


  beforeEach(function () {
    // Instantiate the app module
    angular.mock.module('app');

    // Store references to $compile and $rootScope so they're available to all
    // tests in this describe block
    // Note that the injector unwraps the underscores from around parameter
    // names when matching
    angular.mock.inject(function (
      _$compile_,
      _$rootScope_) {

      var elm = angular.element('<gc-dashboard></gc-dashboard>');

      $compile = _$compile_;
      $rootScope = _$rootScope_;
      element = $compile(elm)($rootScope);
      $rootScope.$digest();

      ctrl = element.controller('gcDashboard');
    });

  });

  it('should exist', function(){
    expect(element).toBeDefined();
  });

  it('should have a dashboard element', function() {
    expect( element[0].id ).toBe('dashboard');
  });
});
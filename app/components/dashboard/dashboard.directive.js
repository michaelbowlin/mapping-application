'use strict';

var DashboardController = require('./dashboard.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.dashboard.directive:dashboard
 * @restrict E
 * @description The directive that handles the dashboard
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-dashboard></data-gc-dashboard>
 * </pre>
 *
 * @ngInject
 */
function gcDashboard() {

  return {
    bindToController: true,
    controller: DashboardController,
    controllerAs: 'dashboard',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/dashboard/dashboard.html'
  };

}

module.exports = gcDashboard;


'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.dashboard
 * @description A module for the dashboard directive
 */
module.exports = angular.module('app.components.dashboard', [])
  .controller('DashboardController',
    require('./dashboard.controller'))
  .directive('gcDashboard',
    require('./dashboard.directive'));

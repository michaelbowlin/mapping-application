'use strict';

/**
 * @ngdoc controller
 * @name app.materialize.dashboard.controller:DashboardController
 * @description Controller for the dashboard module.
 *
 */
class DashboardController {
  /** @ngInject */
  constructor() {
    
    // setup injectibles
    
    /** Set-up the view model */
    let vm = this;

  }

}

module.exports = DashboardController;
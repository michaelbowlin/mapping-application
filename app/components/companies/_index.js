'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.companies
 * @description A module for the companies directive
 */
module.exports = angular.module('app.components.companies', [])
  .controller('CompaniesController',
    require('./companies.controller'))
  .directive('gcCompanies',
    require('./companies.directive'))
  .directive('gcCompanyDetail',
    require('./company.detail.directive'))
  .directive('gcCompanyForm',
    require('./company.form.directive'))
  .directive('gcCompaniesLanding',
    require('./companies.landing.directive'));

'use strict';

var CompaniesController = require('./companies.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.companies.directive:companies
 * @restrict E
 * @description The directive that handles the companies
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-companies></data-gc-companies>
 * </pre>
 *
 * @ngInject
 */
function gcCompanies() {

  return {
    bindToController: true,
    controller: CompaniesController,
    controllerAs: 'companies',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/companies/companies.html'
  };

}

module.exports = gcCompanies;
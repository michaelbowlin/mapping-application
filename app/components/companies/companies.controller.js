'use strict';

const STATE = new WeakMap();

/**
 * @ngdoc controller
 * @name  app.materialize.companies.controller:CompaniesController
 * @description Controller for the companies module.
 *
 */
class CompaniesController {
  /** @ngInject */

  constructor($state) {

    // setup injectibles
    STATE.set(this, $state);

    /** Set-up the view model */
    let vm = this;

    // properties
    this.handleCreate = this._handleCreate;
    this.handleEdit = this._handleEdit;

    this._setupLocalization();
    this._getCurrentState();

    this.currentCompanyName = "PepsiCo";
    this.showValidation = false;

  }

  /**
   * @ngdoc method
   * @name CompaniesController#_getCurrentState
   * @methodOf app.materialize.companies.controller:CompaniesController
   * @description gets the current state, used to set up the view
   */
  _getCurrentState() {
    let state = STATE.get(this);
    this.currentState = state.current.child;
  }

  /**
   * @ngdoc method
   * @name CompaniesController#_handleCreate
   * @methodOf app.materialize.companies.controller:CompaniesController
   * @description handles the creation of a company
   */
  _handleCreate() {
    STATE.get(this).go('COMPANIES.DETAILS', {
      id: 1
    });
  }

  /**
   * @ngdoc method
   * @name CompaniesController#_handleEdit
   * @methodOf app.materialize.companies.controller:CompaniesController
   * @description handles the edit of an exisiting company name
   */
  _handleEdit() {
    STATE.get(this).go('COMPANIES.DETAILS', {
      id: 1
    });
  }

  /**
   * @ngdoc method
   * @name CompaniesController#_setupLocalization
   * @methodOf app.materialize.companies.controller:CompaniesController
   * @description sets up the localization strings for the companies module
   */
  _setupLocalization() {

    // TODO: add localization service call when it's ready

    this.addLabel = "Add";
    this.createCompanyLabel = "Create Company";
    this.createNewCompanyLink = "Create New";
    this.cancelButtonLabel = "Cancel";
    this.createButtonLabel = "Create";
    this.companyDetailLabel = "Company Details";
    this.companyNameLabel = "Company Name";
    this.companyExampleLabel = "i.e. My Company";
    this.deleteLabel = "Delete";
    this.departmentCountLabel = 0;
    this.departmentsLabel = "Departments";
    this.editLabel = "Edit";
    this.editCompanyLabel = "Edit Company";
    this.errorExistingCompany = "This company already exists, please enter a unique company name."
    this.hideLabel = "Hide";
    this.manageCompaniesLabel = "Manage Companies";
    this.projectsCountLabel = 0;
    this.projectsLabel = "Projects";
    this.saveButtonLabel = "Save";
    this.searchBoxLabel = "Lookup by Company Name";
    this.showLabel = "Show";
    this.usersCountLabel = 0;
    this.usersLabel = "Users";
    this.viewExistingCompany = "View exisiting company.";

  }
}

module.exports = CompaniesController;

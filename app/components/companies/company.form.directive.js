'use strict';

var CompaniesController = require('./companies.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.companies.form.directive:companies
 * @restrict E
 * @description The directive that handles the companies form
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-company-form></data-gc-company-form>
 * </pre>
 *
 * @ngInject
 */
function gcCompanyForm() {

  return {
    bindToController: true,
    controller: CompaniesController,
    controllerAs: 'companies',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/companies/company.form.html'
  };

}

module.exports = gcCompanyForm;
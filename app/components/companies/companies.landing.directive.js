'use strict';

var CompaniesController = require('./companies.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.companies.landing.directive:companies
 * @restrict E
 * @description The directive that handles the companies landing
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-companies-landing></data-gc-companies-landing>
 * </pre>
 *
 * @ngInject
 */
function gcCompaniesLanding() {

  return {
    bindToController: true,
    controller: CompaniesController,
    controllerAs: 'companies',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/companies/companies.landing.html'
  };

}

module.exports = gcCompaniesLanding;

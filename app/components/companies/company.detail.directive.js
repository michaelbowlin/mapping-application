'use strict';

var CompaniesController = require('./companies.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.companies.detail.directive:companies
 * @restrict E
 * @description The directive that handles the companies detail
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-company-detail></data-gc-company-detail>
 * </pre>
 *
 * @ngInject
 */
function gcCompanyDetail() {

  return {
    bindToController: true,
    controller: CompaniesController,
    controllerAs: 'companies',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/companies/company.detail.html'
  };

}

module.exports = gcCompanyDetail;

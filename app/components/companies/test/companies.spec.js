/* global angular */

'use strict';

describe('Unit: Companies Component', function () {

  var $compile;
  var ctrl;
  var element;
  var $rootScope;

  // Mock Localization
  var mockLocalization = {
  };


  beforeEach(function () {
    // Instantiate the app module
    angular.mock.module('app');

    // Store references to $compile and $rootScope so they're available to all
    // tests in this describe block
    // Note that the injector unwraps the underscores from around parameter
    // names when matching
    angular.mock.inject(function (
      _$compile_,
      _$rootScope_) {

      var elm = angular.element('<gc-companies></gc-companies>');

      $compile = _$compile_;
      $rootScope = _$rootScope_;
      element = $compile(elm)($rootScope);
      $rootScope.$digest();

      ctrl = element.controller('gcCompanies');
    });

  });

  it('should exist', function(){
    expect(element).toBeDefined();
  });

  it('should have a container with a page-header class', function() {
    expect( element[0].id ).toBe('companies');
  });

  // TODO: You need more tests, they go here
});
'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.components.navbar
 * @description A module for the navbar
 */
module.exports = angular.module('app.components.navbar', [])
  .controller('NavbarController',
    require('./navbar.controller'))
  .directive('gcNavbar',
    require('./navbar.directive'));

'use strict';

const STATE = new WeakMap();

/**
 * @ngdoc controller
 * @name  app.materialize.navbar.controller:NavbarController
 * @requires $state
 * @description Controller for the navbar module
 *
 */
class NavbarController {
  /** @ngInject */
  constructor( $state ) {

    // setup injectibles
    STATE.set(this, $state);

    /** Set-up the view model */
    let vm = this;

    vm.isActive = this._isActive;

    this._setupLocalization();
  }

  /**
   * @ngdoc method
   * @name NavbarController#_isActive
   * @methodOf app.materialize.navbar.controller:NavbarController
   * @description returns a boolean whether the users current page matches a 
   * link in the navbar
   * @param {String} page The page/link in the navbar being evaluated
   */
  _isActive( page ) {
    let active = (page === STATE.get(this).current.name);
    return active;
  }

  /**
   * @ngdoc method
   * @name NavbarController#_setupLocalization
   * @methodOf app.materialize.navbar.controller:NavbarController
   * @description sets up the localization strings for the Navbar
   */
  _setupLocalization() {

    // TODO: add localization service call when it's ready
    this.admin = "Admin";
    this.dashboard = "Dashboard";
    this.map = "Map";
    this.projects = "Projects";
    this.reporting = "Reporting";

  }

}

module.exports = NavbarController;

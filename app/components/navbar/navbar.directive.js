'use strict';

var NavbarController = require('./navbar.controller.js');

/**
 * @ngdoc directive
 * @name app.materialize.navbar.directive:navbar
 * @restrict E
 * @description The directive that handles the top navbar
 *
 * @example
 * Here's an example of how to use the directive
 * <pre>
 * <data-gc-navbar></data-gc-navbar>
 * </pre>
 *
 * @ngInject
 */
function gcNavbar() {

  return {
    bindToController: true,
    controller: NavbarController,
    controllerAs: 'navbar',
    replace: true,
    restrict: 'E',
    scope: {},
    templateUrl: 'components/navbar/navbar.html'
  };

}

module.exports = gcNavbar;


/* global angular */

'use strict';

describe('Unit: Navbar Component', function() {

  var $compile;
  var ctrl;
  var element;
  var $rootScope;

  // Mock Localization
  var mockLocalization = {
    'navbar.admin': 'Admin',
    'navbar.dashboard': 'Dashboard',
    'navbar.projects': 'Projects',
    'navbar.reporting': 'Reporting'
  };


  beforeEach(function() {
    // Instantiate the app module
    angular.mock.module('app');

    // Store references to $compile and $rootScope so they're available to all
    // tests in this describe block
    // Note that the injector unwraps the underscores from around parameter
    // names when matching
    angular.mock.inject(function(
      _$compile_,
      _$q_,
      _$rootScope_
    ) {

      var elm = angular.element('<gc-navbar></gc-navbar>');

      $compile = _$compile_;
      $rootScope = _$rootScope_;
      element = $compile(elm)($rootScope);
      $rootScope.$digest();

      ctrl = element.controller('gcNavbar');
    });

  });

  it('should exist', function() {
    expect(element).toBeDefined();
  });

  it('should have a nav container with class: navbar', function() {
    expect(element[0].className.indexOf('navbar')).toBeGreaterThan(-1);
  });

});

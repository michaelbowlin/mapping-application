'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.message
 * @description Bootstrapper for the message module
 */
module.exports = angular.module('app.message', [])
  .config(require('./message.config'));

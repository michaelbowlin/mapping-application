'use strict';

/**
 * State declarations
 */
var message = {
  name: 'MESSAGE',
  // change URL param to query string so we don't have to double URI encode
  url: '/message/{errorCode}',
  template: '<data-gc-message></data-gc-message>',
  title: 'Message'
};

/*  @ngInject */
function messageConfig($stateProvider) {

  $stateProvider
    .state(message);

}

module.exports = messageConfig;
'use strict';

// For support of Maps, Sets, WeakMaps, and WeakSets
require('babelify/polyfill');

var angular = require('angular');

// Attach lodash to the global scope
global._ = require('lodash');

// Angular 3rd party libraries
require('angular-local-storage');
require('angular-bootstrap');
require('angular-ui-router');

// Other 3rd party libraries
require('restangular');
//require('google-locations');

// GC libraries
// require('gc-ui-generic-materialize');
// require('gc-ui-techcheck');
// require('gc-ui-validators');

// App modules
// require('./companies/_index');
require('./components/_index');
require('./dashboard/_index');
require('./map/_index');
// require('./landing/_index');
require('./login/_index');
require('./message/_index');
require('./resetPassword/_index');
require('./templates');

// create and bootstrap application
angular.element(document).ready(function () {

  var requires = [
    // Angular 3rd party libraries
    'LocalStorageModule',
    'ui.bootstrap',
    'ui.router',
    // Other 3rd party libraries
    //'google.locations',
    'restangular',
    // GC Libraries
    // 'gc.ui.materialize.generic',
    // 'gc.ui.materialize.generic.templates',
    // 'gc.ui.materialize.techcheck',
    // 'gc.ui.materialize.validators',
    // Our app's modules
    // 'app.companies',
    'app.components',
    'app.dashboard',
    'app.map',
    // 'app.landing',
    'app.login',
    'app.message',
    'app.resetPassword',
    'templates'
  ];

  // mount on window for testing
  window.app = angular.module('app', requires);

  angular.module('app')
    .constant('AppSettings', require('./config/constants'))
    .constant('EnvSettings', require('./config/envConstants'))
    .constant('LocalMsgs', require('./config/localMessages'))
    .config(require('./config/routes'))
    .run(require('./config/on_run'));

  angular.bootstrap(document, ['app']);

});
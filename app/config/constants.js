'use strict';

var AppSettings = {
  appTitle: 'My Company',
  apiUrl: '',
  apiTimeout: 15000
};

module.exports = AppSettings;
'use strict';

/*  @ngInject */
function OnRun(
  $rootScope,
  $state,
  Restangular,
  AppSettings,
  EnvSettings
) {
  
  // Make sure the user has a valid browser
  // if ( ! techCheckService.isValidBrowser() ) {
  //   $state.go('MESSAGE', { errorCode: 'BROWSER' } );
  // }

  // change page title based on state
  $rootScope.$on('$stateChangeSuccess', (event, toState) => {

    $rootScope.gaTrackingId = EnvSettings.gaTrackingId;
    $rootScope.pageTitle = '';

    if ( toState.title ) {
      $rootScope.pageTitle += toState.title;
      $rootScope.pageTitle += ' \u2015 ';
    }

    $rootScope.pageTitle += AppSettings.appTitle;

  });
  
  // The global Restangular error interceptor for API error response codes
  Restangular.setErrorInterceptor (
    // TODO: add third argument back in if needed: responseHandler
    (response, deferred) => {
      var errorCode;

      switch (response.status) {
        case 0:
          console.warn('app.run caught a 0 (probably 404) response from  an API call...',
            response);
          
          // get the message and possible URL out of the response body
          errorCode = 'MISSING_URL';
          
          // go to the message state and pass the parameters needed on the URL
          $state.go('MESSAGE', {
            errorCode: errorCode
          });
          
          // reject the promise in case of any cleanup needed
          deferred.reject(response);

          return false;
        case 401:
          console.info('app.run caught a 401 response from  an API call...',
            response);

          // get the message and possible URL out of the response body
          errorCode = response.data.errorCode;

          // reject the promise in case of any cleanup needed
          deferred.reject(response);

          return true;
        // TODO: figure out if we want to globally trap this case due to
        // permissions
        case 403:
          console.info('app.run caught a 403 response from an API call...',
            response);
          
          // get the message and possible URL out of the response body
          errorCode = response.data.errorCode;
          
          // go to the message state and pass the parameters needed on the URL
          $state.go('MESSAGE', {
            errorCode: errorCode,
            url: response.data.redirectUrl || ''
          });
          
          // reject the promise in case of any cleanup needed
          deferred.reject(response);

          return false;
        case 404:
          console.info('app.run caught a 404 response from an API call...',
            response);
          
          // get the message and possible URL out of the response body
          errorCode = 'MISSING_URL';
          
          // go to the message state and pass the parameters needed on the URL
          $state.go('MESSAGE', {
            errorCode: errorCode
          });
          
          // reject the promise in case of any cleanup needed
          deferred.reject(response);

          return false;
        case 500:
          console.error('app.run caught a 500 response from an API call...',
            response);
          
          // get the message and possible URL out of the response body
          errorCode = 'BAD_URL';
          
          // go to the message state and pass the parameters needed on the URL
          $state.go('MESSAGE', {
            errorCode: errorCode,
            url: response.data.redirectUrl || ''
          });
          
          // reject the promise in case of any cleanup needed
          deferred.reject(response);

          return false;
        default:
          return true;

      }
      
    }
  );
}

module.exports = OnRun;

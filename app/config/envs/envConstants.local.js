'use strict';

/**
 * Dev environmental-specific configuration
 */
var EnvSettings = {
  gaTrackingId: '',
  apiHost: 'http://rcd-appraisals-dev.azurewebsites.net/api/',
  //apiHost: 'http://localhost',
  facebookAppId: ''
};

module.exports = EnvSettings;
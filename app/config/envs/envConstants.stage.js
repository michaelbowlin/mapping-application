'use strict';

/**
 * Stage environmental-specific configuration
 */
var EnvSettings = {
  gaTrackingId: '',
  apiHost: '',
  facebookAppId: ''
};

module.exports = EnvSettings;
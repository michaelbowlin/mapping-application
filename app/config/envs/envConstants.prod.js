'use strict';

/**
 * Prod environmental-specific configuration
 */
var EnvSettings = {
  gaTrackingId: '',
  apiHost: '',
  facebookAppId: ''
};

module.exports = EnvSettings;
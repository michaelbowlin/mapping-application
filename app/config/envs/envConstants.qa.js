'use strict';

/**
 * QA environmental-specific configuration
 */
var EnvSettings = {
  gaTrackingId: '',
  apiHost: '',
  facebookAppId: ''
};

module.exports = EnvSettings;
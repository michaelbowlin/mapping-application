'use strict';

/*  @ngInject */
function Routes($locationProvider,
                $urlRouterProvider,
                AppSettings,
                EnvSettings,
                RestangularProvider) {

  RestangularProvider.setBaseUrl(EnvSettings.apiHost);

  // TODO: need to decide how long we want Restangular timeout to be
  RestangularProvider.setDefaultHttpFields({
    // TODO: see if we can use this to cache results to lessen the API requests
    // cache: true,
    timeout: AppSettings.apiTimeout
  });

  RestangularProvider.setRequestInterceptor((elem, operation) => {
    if (operation === "remove") {
       return undefined;
    } 
    return elem;
  });

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });

  $urlRouterProvider.otherwise('/');

}

module.exports = Routes;
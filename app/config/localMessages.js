'use strict';

var LocalMsgs = {
  message: {
    'BROWSER.title': 'Browser Error...',
    'BROWSER.body': 'Please upgrade your browser to {browser} ' +
      '{version} or higher to view this website.',
    'SERVER_ERROR.title': 'Sorry...',
    'SERVER_ERROR.body': 'Oops, we are having technical issues. ' +
      'Please try again at a later time by clicking the link provided in ' +
      'your email.',
    'BAD_URL.title': 'Sorry...',
    'BAD_URL.body': 'Oops, we are having technical issues. ' +
      'Please try again later.',
    'MISSING_URL.title': 'Sorry...',
    'MISSING_URL.body': 'Oops, looks like that page does not ' +
      'exist. Please double-check that the link provided in your email is ' +
      'correct in your browser.'
  },
  general: {
    'footer.gc.copyright': 'Brainyak, Inc. All rights reserved.',
    'footer.privacyPolicy': 'Privacy Policy'
  }
};

module.exports = LocalMsgs;
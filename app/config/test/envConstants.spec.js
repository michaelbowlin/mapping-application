/* global angular */

'use strict';

describe('Unit: Environmental Constants', function () {

  var envConstants;

  beforeEach(function () {
    // instantiate the app module
    angular.mock.module('app');

    // mock the component
    angular.mock.inject(function (EnvSettings) {
      envConstants = EnvSettings;
    });
  });

  it('should exist', function () {
    expect(envConstants).toBeDefined();
  });

  it('should have a Google Analytics tracking ID defined', function () {
    expect(envConstants.gaTrackingId).toBeDefined();
  });

  it('should have an API host defined', function () {
    expect(envConstants.apiHost).toBeDefined();
  });

});
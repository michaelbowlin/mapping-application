/* global angular */

'use strict';

describe('Unit: Constants', function () {

  var constants;

  beforeEach(function () {
    // instantiate the app module
    angular.mock.module('app');

    // mock the component
    angular.mock.inject(function (AppSettings) {
      constants = AppSettings;
    });
  });

  it('should exist', function () {
    expect(constants).toBeDefined();
  });

  it('should have an application name', function () {
    expect(constants.appTitle).toEqual('My Company');
  });

});
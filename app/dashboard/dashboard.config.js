'use strict';

/**
 * State declarations
 */
var dashboard = {
  name: 'DASHBOARD',
  template: '<data-gc-dashboard></data-gc-dashboard>',
  title: 'Dashboard',
  url: '/dashboard'
};

/*  @ngInject */
function dashboardConfig($stateProvider) {

  $stateProvider
    .state(dashboard);

}

module.exports = dashboardConfig;
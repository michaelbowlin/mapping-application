'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.dashboard
 * @description Bootstrapper for the dashboard module
 */
module.exports = angular.module('app.dashboard', [])
  .config(require('./dashboard.config'));

'use strict';

/**
 * State declarations
 */
var login = {
  name: 'LOGIN',
  template: '<data-gc-login></data-gc-login>',
  title: 'Login',
  url: '/login'
};

/*  @ngInject */
function loginConfig($stateProvider) {

  $stateProvider
    .state(login);

}

module.exports = loginConfig;
'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.login
 * @description Bootstrapper for the login module
 */
module.exports = angular.module('app.login', [])
  .config(require('./login.config'));

'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.map
 * @description Bootstrapper for the map module
 */
module.exports = angular.module('app.map', [])
  .config(require('./map.config'));

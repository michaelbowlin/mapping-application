'use strict';

/**
 * State declarations
 */
var map = {
  name: 'MAP',
  template: '<data-gc-map></data-gc-map>',
  title: 'Map',
  url: '/map'
};

/*  @ngInject */
function mapConfig($stateProvider) {

  $stateProvider
    .state(map);

}

module.exports = mapConfig;
'use strict';

var angular = require('angular');

/**
 * @ngdoc overview
 * @name app.resetPassword
 * @description Bootstrapper for the reset password module
 */
module.exports = angular.module('app.resetPassword', [])
  .config(require('./resetPassword.config'));

'use strict';

/**
 * State declarations
 */
var resetPassword = {
  name: 'RESETPASSWORD',
  params: {
    token: null,
    email: null
  },
  reloadOnSearch: true,
  template: '<data-gc-reset-password></data-gc-reset-password>',
  title: 'Reset Password',
  url: '/resetPassword?token&email'
};

/*  @ngInject */
function resetPasswordConfig($stateProvider) {

  $stateProvider
    .state(resetPassword);

}

module.exports = resetPasswordConfig;